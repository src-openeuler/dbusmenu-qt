%undefine __cmake_in_source_build

Name:           dbusmenu-qt
Version:        0.9.3
Release:        0.20.20150604
Summary:        Dbusmenu library for Qt
License:        LGPLv2+
URL:            https://launchpad.net/libdbusmenu-qt/
Source0:        libdbusmenu-qt-%{version}-20150604bzr.tar.gz

BuildRequires:  cmake dbus-x11 doxygen gcc-c++ pkgconfig pkgconfig(QJson) pkgconfig(QtDBus)
BuildRequires:  pkgconfig(QtGui) pkgconfig(Qt5DBus) pkgconfig(Qt5Widgets) xorg-x11-server-Xvfb

Provides:       libdbusmenu-qt = %{version}-%{release}

%description
Dbusmenu-qt is a library.It provides a Qt implementation of the DBusMenu protocol which could
make applications be able to export and import their menus over DBus.

%package        devel
Summary:        Libraries,head files and other devolopment documents for dbusmenu-qt
Provides:       libdbusmenu-qt-devel = %{version}-%{release}
Requires:       %{name} = %{version}-%{release}

%description    devel
Dbusmenu-qt-devel provides Libraries,head files and other devolopment documents for dbusmenu-qt.

%package        help
Summary:        Documents for dbusmenu-qt
BuildArch:      noarch
Provides:       dbusmenu-qt-doc = %{version}-%{release}
Obsoletes:      dbusmenu-qt-doc < %{version}-%{release}
Conflicts:      dbusmenu-qt-devel < 0.9.3

%description    help
API info and other related files for dbusmenu-qt

%package   -n   dbusmenu-qt5
Summary:        Dbusmenu library for Qt5
Provides:       libdbusmenu-qt5 = %{version}-%{release}

%description -n dbusmenu-qt5
Dbusmenu-qt is a library.It provides a Qt5 implementation of the DBusMenu protocol which could
make applications be able to export and import their menus over DBus.

%package   -n   dbusmenu-qt5-devel
Summary:        Libraries,head files and other devolopment documents for dbusmenu-qt5
Provides:       libdbusmenu-qt5-devel = %{version}-%{release}
Requires:       dbusmenu-qt5 = %{version}-%{release}

%description -n dbusmenu-qt5-devel
Dbusmenu-qt5-devel provides Libraries,head files and other devolopment documents for dbusmenu-qt5.

%prep
%autosetup -n libdbusmenu-qt-%{version} -p1

%build
%global _vpath_builddir qt4
%cmake -DUSE_QT4:BOOL=ON -DUSE_QT5:BOOL=OFF -DWITH_DOC:BOOL=ON
%cmake_build

%global _vpath_builddir qt5
%cmake -DUSE_QT4:BOOL=OFF -DUSE_QT5:BOOL=ON -DWITH_DOC:BOOL=OFF
%cmake_build

%install
%global _vpath_builddir qt4
%cmake_install

%global _vpath_builddir qt5
%cmake_install

rm -rf %{buildroot}%{_docdir}/libdbusmenu-qt*-doc

%check
export PKG_CONFIG_PATH=%{buildroot}%{_libdir}/pkgconfig
test "$(pkg-config --modversion dbusmenu-qt)" = "0.9.2"
test "$(pkg-config --modversion dbusmenu-qt5)" = "0.9.2"
export CTEST_OUTPUT_ON_FAILURE=1
xvfb-run -a dbus-launch --exit-with-session make -C qt4 check ARGS="--output-on-failure --timeout 300" ||:

%files
%license COPYING
%{_libdir}/libdbusmenu-qt.so.2*

%files devel
%{_includedir}/dbusmenu-qt/
%{_libdir}/libdbusmenu-qt.so
%{_libdir}/cmake/dbusmenu-qt/
%{_libdir}/pkgconfig/dbusmenu-qt.pc

%files help
%doc README
%doc qt4/html/

%files -n dbusmenu-qt5
%license COPYING
%{_libdir}/libdbusmenu-qt5.so.2*

%files -n dbusmenu-qt5-devel
%{_includedir}/dbusmenu-qt5/
%{_libdir}/libdbusmenu-qt5.so
%{_libdir}/pkgconfig/dbusmenu-qt5.pc
%{_libdir}/cmake/dbusmenu-qt5/

%changelog
* Thu Nov 14 2024 Funda Wang <fundawang@yeah.net> - 0.9.3-0.20.20150604
- adopt to new cmake macro

* Tue Dec 3 2019 zhouyihang <zhouyihang1@huawei.com> - 0.9.3-0.19.20150604
- Package init
